'use strict';

const TableName = 'User'

module.exports = {
  up: async (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert(TableName, [
     {
       id: 1,
       username: 'yorkholm',
       fullname: 'Yorkholm',
       email: 'yorkholm@hotmail.com',
       phone_number: '+62-811111111',
       password: '$argon2i$v=19$m=4096,t=3,p=1$uuw4jAzGtYDrFNTEzZzR8A$8d+y2tq+l8FzJxVVervEYwvutJhBXlHRF8Hjti8UGcU',
       verification_preference: 'EMAIL',
       user_status: 'ACTIVE',
       google_auth: 'FUZQQMATMRGQMUSX'
     }
   ])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(TableName, null, {})
  }
};