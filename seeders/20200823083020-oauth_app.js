'use strict';

const TableName = 'Oauth_App'

module.exports = {
  up: async (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert(TableName, [
     {
       id: 1,
       identifier: 'auqey-web',
       name: 'AuQey Web',
       comment: 'Our web application used to manage your account connections and tokens',
       our_private: 'private/auqey.key',
       their_public: '--- BEGIN PUBLIC KEY --- --- END PUBLIC KEY---',
       algorithm: 'RS256',
       status: 'VALID'
     },
     {
       id: 2,
       identifier: 'auqey-mobile',
       name: 'AuQey Mobile',
       comment: 'Our mobile application used to manage your account connections and tokens',
       our_private: 'private/auqey.key',
       their_public: '--- BEGIN PUBLIC KEY --- --- END PUBLIC KEY---',
       algorithm: 'RS256',
       status: 'VALID'
     },
     // Add your applications that uses this SSO by registering it here and associate the JWT_Key to this App
   ])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(TableName, null, {})
  }
};