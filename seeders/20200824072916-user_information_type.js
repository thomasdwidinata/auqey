'use strict';

const TableName = 'User_Information_Type'

module.exports = {
  up: async (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert(TableName, [
     {
       id: 1,
       name: 'Home Address',
       internal_id: 'HOME_ADDRESS',
       comment: 'Home Address'
     },
     {
       id: 2,
       name: 'Workplace',
       internal_id: 'WORKPLACE',
       comment: 'User\'s Workplace'
     },
     {
       id: 3,
       name: 'Phone Number',
       internal_id: 'PHONE_NUMBER',
       comment: 'Additional User\'s Phone Numbers'
     }
   ])
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(TableName, null, {})
  }
};