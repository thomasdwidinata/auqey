import { authenticator } from 'otplib';

import { Global } from '../../globals'

export function GenerateGAuthSalt() {
    return authenticator.generateSecret()
}

export function GenerateTokenGAuth(user: string, salt: string): string {
    return authenticator.keyuri(user, Global.app.app_name, salt)
}

export function VerifyTokenGAuth(secret: string, token: string): boolean {
    return authenticator.verify({secret, token})
}

export function GenerateBackupCodesGAuth(user: string, salt: string) {
    // WIP: Implementation of Backup Codes
    return [
        '111111111',
        '222222222',
        '111111111',
        '222222222',
        '111111111',
        '222222222',
        '111111111',
        '222222222',
        '222222222',
        '222222222',
    ]
}