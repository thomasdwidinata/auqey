import argon2 from 'argon2'
import { nanoid } from "nanoid";
import RandomNumber from 'random-number-csprng'

export async function ArgonVerify(hash: string, password: string): Promise<boolean> {
    let Response: boolean = false

    Response = await argon2.verify(hash, password)

    return Response
}

export async function ArgonHash(password: string): Promise<string> {
    let Response: string = ''

    Response = await argon2.hash(password, {
        type: argon2.argon2i,
        timeCost: 50
    })

    return Response
}

export function GenerateToken(length: number = 64): string {
    return nanoid(length)
}

export async function GenerateOTP(min: number = 100000, max: number = 999999): Promise<number> {
    return await RandomNumber(min, max)
}