'use strict'

import { Response } from 'express'
import StatusCodeInterface from '../../interfaces/StatusCodeInterface'
import ResponseInterface from '../../interfaces/ResponseInterface'
import StatusCode from '../../globals/StatusCode'
import ServiceRepsonseInterface from '../../interfaces/ServiceResponseInterface'
import Config from '../../config'

function sender(res: Response<any>, status: StatusCodeInterface, data?: ServiceRepsonseInterface) {
    let response: ResponseInterface = {
        code: status.status_code,
        message: status.message,
        data: data?.data ? data.data : null,
        meta: data?.meta ? data.meta : null,
    }
    if (Config.debug === true && data?.exception) {
        if (response.meta) response.meta.exception = data?.exception
        else response.meta = { exception: data?.exception }
    }
    if (typeof (data?.meta?.status) !== 'undefined') {
        if (response.meta) response.meta.status = data?.meta?.status
        else response.meta = { status: data?.meta?.status }
    }
    return res.status(status.http_code).json(response)
}

export function SuccessHandler(res: Response<any>, data?: ServiceRepsonseInterface) {
    let status: StatusCodeInterface = data?.statusCode ? data.statusCode : (
        data?.data === null ? StatusCode.no_content : StatusCode.success
    )

    return sender(res, status, data)
}

export function MultipleStatusHandler(res: Response<any>, data?: ServiceRepsonseInterface) {
    let status: StatusCodeInterface = data?.statusCode ? data.statusCode : StatusCode.multi_status
    return sender(res, status, data)
}

export function ErrorHandler(res: Response<any>, data?: ServiceRepsonseInterface) {
    let status: StatusCodeInterface = data?.statusCode ? data.statusCode : StatusCode.internal_server_error
    return sender(res, status, data)
}