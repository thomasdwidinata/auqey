import Moment from 'moment'

import LogInterface from '../../interfaces/LogInterface'
import LogTypes from '../../types/LogTypes'

import { BaseLogExtension } from './LogExtensions/BaseLogExtension'
import { FileLogExtension } from './LogExtensions/FileLogExtension'
import { SqliteLogExtension } from './LogExtensions/SqliteLogExtension'

class LogServices {
    private dateTimeFormat: string = 'YYYY-MM-DD HH:mm:ss' 
    
    private icon = {
        'debug': '🔨',
        'log': '',
        'info': 'ℹ️',
        'warn': '⚠️',
        'error': '❌'
    }

    private Logger: BaseLogExtension

    public constructor(type: LogTypes) {
        this.Logger = new FileLogExtension() // For the sake of error proof
        try {
            switch(type) {
                case 'file':
                    this.Logger = new FileLogExtension()
                    break
                case 'mysql':
                case 'sequelize':
                    console.info(`[  ℹ️  ] Log Services: mysql and sequelize is coming soon... Now will be using Sqlite...`)
                case 'sqlite':
                    this.Logger = new SqliteLogExtension()
                    break
            }
        } catch (e) {
            console.error('[  ❌  ] Log Services: Failed to initialise Log Services: ', e)
        }
    }

    public log(message: string, data?: any) { // Shorthand of LogService.write({verbosity: 'log', data: <any>})
        try {
            this.write({
                verbosity: 'log',
                message: message,
                data: data
            })
        } catch (e) {
            console.error('[  ❌  ] Log Services: Error logging:', e)
        }
    }

    public info(message: string, data?: any) { // Shorthand of LogService.write({verbosity: 'info', data: <any>})
        try {
            this.write({
                verbosity: 'info',
                message: message,
                data: data
            })
        } catch (e) {
            console.error('[  ❌  ] Log Services: Error logging:', e)
        }
    }

    public write(data: LogInterface) {
        try {
            if (!data.verbosity) data.verbosity = 'log'
            let output: string = data.verbosity !== 'log' ? `[  ${ data.icon || this.icon[data.verbosity] }  ] { ${ Moment().format(this.dateTimeFormat) } } ( ${data.id || ''} )${data.title ? ` ${data.title}: ` : ''}` : '' // For the sake of performance
            let outputData: string = data.message || ''
            try {
                outputData += JSON.stringify(data.data) || ''
            } catch (e) {
                outputData += data.data.toString() || ''
            }
            
            if (data.message) console[data.verbosity](output, data.message + (data.data ? data.data : ''))
            else if (data.data) console[data.verbosity](output, data.data)

            this.Logger.write(`${output} ${outputData}`)
        } catch (e) {
            console.error('[  ❌  ] Log Services: Error logging:', e)
        }
    }
}
export let LogService: LogServices
export function InitLogService(type: LogTypes) { // Must be called on main function
    LogService = new LogServices(type)
}