'use strict'

import { Express } from 'express'
import { ErrorHandler as EH } from './ResponseHandler'
import StatusCode from '../../globals/StatusCode'

export function ErrorHandler(app: Express) {
    // 404
    app.use(function (req, res, next) {
        return EH(res, {
            statusCode: StatusCode.not_found
        });
    })
    
    // 500
    app.use(function (err: any, req: any, res: any, next: any) {
        // WIP: Implement Error Filter Service
        res.locals.message = err.message;
        return EH(res, {
            statusCode: StatusCode.internal_server_error,
            exception: err
        })
    })
}

