export abstract class BaseLogExtension {
    abstract write(data: string, ticket?: string): any
}