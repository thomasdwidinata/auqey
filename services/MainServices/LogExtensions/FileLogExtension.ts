import {
    createReadStream,
    createWriteStream,
    readFileSync,
    existsSync,
    unlinkSync,
    writeFileSync,
    statSync,
    appendFileSync
} from 'fs'

import { createGzip, unzip } from 'zlib'
import { pipeline } from 'stream'
import { promisify } from 'util'

import Moment from 'moment'

import Config from '../../../config'

import { BaseLogExtension } from './BaseLogExtension'

export class FileLogExtension extends BaseLogExtension {
    maxFileSize: number = Config.max_log_size
    path: string = './app.log'

    public constructor() {
        super()
        this.init()
    }

    public write(data: string, ticket?: string): any {
        this.checkFileSize().then( _ => {
            appendFileSync(this.path, `[${Moment().format('YYYY-MM-DD HH:mm:ss')}] (${ticket ? ticket : ''}) : ${data}\n`);
        })
    }
    
    private async init() {
        if(existsSync(this.path)) {
            const stats = statSync(this.path).size / 1000000.0
            if(stats >= this.maxFileSize) {
                await this.compress(this.path)
                unlinkSync(this.path)
                writeFileSync(this.path, '')
            }
        } else writeFileSync(this.path, '')
    }
    
    public async compress(path: string) {
        const pipe = promisify(pipeline)
        
        async function do_gzip(input: string, output: string) {
          const gzip = createGzip()
          const source = createReadStream(input)
          const destination = createWriteStream(output)
          await pipe(source, gzip, destination)
        }
        
        try {
            await do_gzip(path, `${path}_${Moment().toISOString()}.gz`)
            return true
        } catch (e) {
            console.error('[  ❌  ] Log Services: Failed to compress file log: ', e)
            return false
        }
    }
    
    public async decompress(path: string) {
        const buffer = readFileSync(path)
        const do_unzip = promisify(unzip)
    
        try {
            let uncompressed: any = await do_unzip(buffer)
            console.log(uncompressed.toString('utf8'))
            return uncompressed
        } catch (e) {
            console.error('[  ❌  ] Log Services: Failed to decompress file log: ', e)
        }
    }

    async checkFileSize() {
        try {
            const stats = statSync(this.path).size / 1000000.0
            if(stats >= this.maxFileSize)
                await this.init()
        } catch (e) {
            console.error(`[  ❌  ] Log Services: Error checking size of the log file: `, e)
        }
    }

}