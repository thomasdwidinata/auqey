// import sqlite3 from 'sqlite3'
import { Database } from 'sqlite3'
import { BaseLogExtension } from './BaseLogExtension'

export class SqliteLogExtension extends BaseLogExtension {
    private filePath: string = './app_log.sqlite'

    private instance: Database

    public constructor() {
        super()
        this.instance = new Database(this.filePath)
        this.initTable()
    }

    public write(data: string, ticket?: string): any {
        let statement = this.instance.prepare(`INSERT INTO Logs (log ${ticket ? ', ticket' : ''}) VALUES (? ${ticket ? ',?' : ''})`)
        statement.run(data)
        statement.finalize()
    }

    private initTable() {
        this.instance.serialize( () => {
            this.instance.run(`CREATE TABLE IF NOT EXISTS Logs (
                id INTEGER NOT NULL PRIMARY KEY,
                log TEXT NOT NULL,
                ticket TEXT,
                created_at INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP
            )`)
        } )
    }

}