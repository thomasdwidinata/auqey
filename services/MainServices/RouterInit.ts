'use strict'

import { Express, Router } from 'express'
import { readdirSync } from 'fs'

// Disabled: since this API will be all public due to this is an SSO API. Comment the line above and uncomment below if you need to use this API as a daily basis with authentication
// import AuthenticationMiddleware from '../../middlewares/AuthenticationMiddleware'

export function RouteScan(app: Express) {
    const routesDir = __dirname + '/../../routes/'
    let routes = TraverseRoutes(routesDir)
    for (let route of routes)
        RouteInjector(app, route)
    return routes
}

function TraverseRoutes(dir: string) {
    let routes: any[] = []
    let routeDir = dir.split('/routes/')[dir.split('/routes/').length-1]
    readdirSync(dir, { withFileTypes: true })
        .filter( (file) => {
            if (file.isFile()) return file.name.slice(-3) === '.js'
            return true
        })
        .forEach( (file) => {
            if (file.isDirectory()) {
                let currentDir = file.name
                let childs = TraverseRoutes(`${dir}${currentDir}/`)
                routes = routes.concat(childs)
            } else {
                // Disabled: since this API will be all public due to this is an SSO API. Comment the line above and uncomment below if you need to use this API as a daily basis with authentication
                // let access_type = file.name.split('_')[0]
                // let routeName = file.name.split('_')[file.name.split('_').length-1].split('.').slice(0, -1).join('.')

                let routeName = file.name.split('.').slice(0, -1).join('.')

                routes.push({
                    url: `/${routeDir === '' ? `${file.name}` : `${routeDir}${routeName}`}`,
                    controller: require(`${dir}${file.name}`)
                })

                // Disabled: since this API will be all public due to this is an SSO API. Comment the line above and uncomment below if you need to use this API as a daily basis with authentication
                // switch(access_type) {
                //     case 'public':
                //         routes.push({
                //             url: `/${routeDir === '' ? `${file.name}` : `${routeDir}${routeName}`}`,
                //             controller: require(`${dir}${file.name}`)
                //         })
                //     default: // Default always use middleware
                //         routes.push({
                //             url: `/${routeDir === '' ? `${file.name}` : `${routeDir}${routeName}`}`,
                //             controller: require(`${dir}${file.name}`),
                //             middleware: AuthenticationMiddleware
                //         })
                // }
                
            }
        })
    return routes
}

export function RouteInjector(app: Express, route: { url: string; controller: Router, middleware?: Router }) {
    try { // WIP: Error while using app.use() as route.controller was encapsulated into default but Typescript not accessible
        if (typeof(route.middleware) !== 'undefined')
            app.use(route.url, route.middleware, route.controller)
        else
            app.use(route.url, route.controller)
    } catch (e) {
        console.error(`Unable to use route: ${route.url}`)
        if (typeof(route.middleware) !== 'undefined') console.error(`with a middleware.`)
        console.error(`Error: `, e)
    }
}