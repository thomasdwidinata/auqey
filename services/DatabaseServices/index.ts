import { Sequelize, Model } from 'sequelize'
import Options from 'sequelize'
import { readdirSync } from 'fs'

import Config from '../../config'
import { LogService } from '../MainServices/LogServices'
import Relations from '../../model/Relations'

export const ServiceTitle: string = 'Database Service'

class DatabaseServices {
    instance: Sequelize

    modelPath: string = __dirname + '/../../model/'
    models: any[]

    constructor() {
        this.instance = new Sequelize(Config.database.name, Config.database.username, Config.database.password, {
            host: Config.database.host,
            port: Config.database.port as number,
            dialect: Config.database.dialect as Options.Dialect || 'mysql',
            // dialectOptions: {
            //     dateStrings: true,
            //     typeCast: true
            // },
            logging: Config.debug ? console.log : this.DbLog // Idk which implementation is the best, but I will just stick with this...
        })

        LogService.write({
            verbosity: 'info',
            icon: '🔍',
            message: 'Scanning Models...',
            title: ServiceTitle
        })
        this.models = this.loadModels()

        LogService.write({
            verbosity: 'info',
            icon: '⏳',
            message: 'Syncing Models...',
            title: ServiceTitle
        })
        this.syncModels(true)

        LogService.write({
            verbosity: 'info',
            icon: '🔍',
            message: 'Connecting to Database...',
            title: ServiceTitle
        })
        this.validateConnection()
    }

    loadModels() {
        let models: Model[] = []
        readdirSync(this.modelPath, { withFileTypes: true })
            .filter( dirent => dirent.isFile() && dirent.name.slice(-3) === '.js' )
            .map( dirent => dirent.name )
            .forEach( file => {
                let currentModel = require(`${this.modelPath}${file}`)
                currentModel = currentModel.LoadModel(this.instance)
                models.push(currentModel)
            })
        Relations()
        return models
    }

    async syncModels(alter: boolean = false) {
        this.instance.sync({ alter })

        // If you use Class, you don't need to sync manually. This one is intended if you use sequelize.define
        // for (let model of this.models) {
        //     if (alter) await model.sync({alter: true})
        //     else await model.sync()
        // }
    }

    async validateConnection() {
        try {
            await this.instance.authenticate()
            LogService.write({
                verbosity: 'info',
                icon: '✅',
                message: 'Database connection verified',
                title: ServiceTitle
            })
        } catch (e) {
            LogService.write({
                verbosity: 'error',
                message: 'Unable to connect to Database',
                data: e,
                title: ServiceTitle
            })
        }
    }

    closeConnection() {
        this.instance.close()
    }

    DbLog(data: any) {
        LogService.write({
            verbosity: 'info',
            title: 'Database Services',
            icon: '📖',
            data
        })
    }
}

export let DatabaseInstance: DatabaseServices
export function InitDatabase() {
    DatabaseInstance = new DatabaseServices()
}