import { Request, Response } from 'express'
import { ErrorHandler, SuccessHandler } from './MainServices/ResponseHandler'
import StatusCode from '../globals/StatusCode'

export function EndControllerServices(req: Request<any>, res: Response<any>, next: Function) {
    if (res.locals.Response) return SuccessHandler(res, res.locals.Response)
    else if (res.locals.Error) return ErrorHandler(res, res.locals.Error)
    else {
        return ErrorHandler(res, {
            data: `API did not end gracefully`,
            statusCode: StatusCode.internal_server_error
        }) 
    }
}