import { createTransport, SendMailOptions } from 'nodemailer'
import { twig, Template } from 'twig'
import { readFileSync } from 'fs'
import Config from '../../config'
import SMTPTransport from 'nodemailer/lib/smtp-transport'

const templatePath = __dirname + '/../../template/'

let MailKey: {client_id?: string, private_key?: string, [key: string]: any} = {}
try {
    MailKey = JSON.parse(readFileSync(Config.mail.oauth2_key_path, 'utf8')) || {}
} catch (e) {
    MailKey = {}
}

let TransporterConfig: SMTPTransport.Options = {
    host: Config.mail.host,
    port: Config.mail.port,
}
if (Config.mail.auth_type === 'oauth') {
    TransporterConfig.secure = true // Always override this when using OAuth2
    TransporterConfig.auth = {
        type: 'OAuth2',
        user: Config.mail.user,
        serviceClient: MailKey.client_id,
        privateKey: MailKey.private_key,
    }
} else {
    TransporterConfig.secure = Config.mail.secure
    TransporterConfig.auth = {
        user: Config.mail.user,
        pass: Config.mail.password
    }
}

const Transporter = createTransport(TransporterConfig)

export function ComposeMail(template: string, data: object): string {
    let input: Template = twig({
        data: readFileSync(`${templatePath}/${template}.html`).toString('utf8')
    })
    let output: string = input.render(data)
    return output
}

export async function DispatchMail(options: SendMailOptions) {
    return await Transporter.sendMail(options).then((e) => {
        console.log(e)
    })
}