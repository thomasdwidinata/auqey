import { SignJWT } from "../../JWTService"
import { randomBytes } from 'crypto'
import { User } from "../../../model/User"
import { User_Token } from "../../../model/User_Token"
import { LogService } from "../../MainServices/LogServices"
import { ServiceTitle } from './LoginServices'
import Moment from 'moment'
import LoginEnum from "../../../enums/AuthenticationEnums/LoginEnum"
import { ArgonVerify } from "../../CryptoServices"
import { Global } from "../../../globals"
import { IdVerified } from "../../../types/DbEnums/IdVerified"

const OauthAppId: number = 1

export async function SignIn(user: User): Promise<string> {
    let refresh_token = await SignJWT({
        id: user.getDataValue('id'),
        username: user.getDataValue('username'),
        hash: randomBytes(64)
    }) // WIP: Making a proper Refresh Token
    let real_token = await SignJWT({
        id: user.getDataValue('id'),
        username: user.getDataValue('username'),
        hash: randomBytes(64)
    })
    let token = await SignJWT({
        id: user.getDataValue('id'),
        username: user.getDataValue('username'),
        refresh_token,
        token: real_token
    })
    let userToken = await User_Token.create({
        UserId: user.getDataValue('id'),
        token,
        OauthAppId,
        token_expiry: Moment().utc().add(15, 'minutes').format(Global.sqlDateTime),
        refresh_token,
        refresh_token_expiry: Moment().utc().add(15, 'minutes').format(Global.sqlDateTime)
    })
    if (!userToken) LogService.write({
        title: ServiceTitle,
        message: `Unable to insert current user's new Token to User_Token`,
        verbosity: 'warn',
        data: {
            user, userToken
        }
    })
    return token
}

export async function CheckPassword(user: User, givenPassword: string): Promise<boolean> {
    let Response = false
    try {
        Response = await ArgonVerify(user.getDataValue('password'), givenPassword)
    } catch (e) {
        LogService.write({
            title: ServiceTitle,
            message: `'Argon2' library could not verify the password`,
            verbosity: 'warn',
            data: e
        })
    }
    return Response
}

export function CheckAccountValidity(user: User): LoginEnum {
    let Response: LoginEnum = LoginEnum.accountDisabled

    if (user.getDataValue('user_status') !== 'ACTIVE') Response = LoginEnum.accountDisabled

    let expiry = user.getDataValue('password_expiry')
    if (expiry) Response = Moment(expiry) < Moment().utc() ? LoginEnum.expiredPassword : LoginEnum.success
    else Response = LoginEnum.success

    return Response
}

export function Check2FA(user: User): boolean {
    return user.getDataValue('verification_preference') ? true : false
}

export function Get2FAs(user: User) {
    return {
        EMAIL: user.getDataValue('email') && user.getDataValue('email_verified') as IdVerified === '2FA' ? true : false,
        PHONE_NUMBER: user.getDataValue('phone_number') && user.getDataValue('phone_number_verified') as IdVerified === '2FA' ? true : false,
        GOOGLE_AUTH: user.getDataValue('google_auth') ? true : false,
        preferece: user.getDataValue('verification_preference')
    }
}