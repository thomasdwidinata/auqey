import { Request, Response } from 'express'
import { LogService } from '../../MainServices/LogServices'
import { ErrorHandler } from '../../MainServices/ResponseHandler'
import Joi from 'joi'
import StatusCode from '../../../globals/StatusCode'
import ValidateBodyEnum from '../../../enums/AuthenticationEnums/ValidateBodyEnum'
import { User } from '../../../model/User'
import LoginEnum from '../../../enums/AuthenticationEnums/LoginEnum'
import { CheckPassword, CheckAccountValidity, SignIn, Check2FA, Get2FAs } from './LoginServicesSupporting'
import { GenerateToken } from '../../CryptoServices'
import { SignJWT } from '../../JWTService'
import { SetRedis } from '../../RedisService'

export const ServiceTitle = 'Login Services'

export function ValidateBody(req: Request<any>, res: Response<any>, next: Function, validateType: ValidateBodyEnum) {
    try {
        const LoginValidation = Joi.object({
            username: Joi.string()
                .min(4)
                .required(),
            password: Joi.string()
                .required()
        })
        
        const TwoFactorAuthValidation = Joi.object({
            otp: Joi.string()
                .min(6)
                .max(8)
                .token()
                .required(),
            type: Joi.string()
                .valid('EMAIL', 'PHONE_NUMBER', 'GOOGLE_AUTH') // Make sure this syncs with the Enums on `User` table
                .required(),
            recovery_token: Joi.string()
                .required()
        })

        const schema: Joi.ObjectSchema = validateType === ValidateBodyEnum.Login ? LoginValidation : TwoFactorAuthValidation

        const { error, value } = schema.validate(req.body)
        if (error) return ErrorHandler(res, {
            data: error,
            statusCode: StatusCode.bad_request
        })

        res.locals.ValidatedBody = value
    } catch (e) {
        const ErrorMessage = `Unable to Insert to table Check for data ${JSON.stringify(req.body)}`
        LogService.write({
            data: ErrorMessage,
            title: ServiceTitle,
            verbosity: 'error'
        })
        return ErrorHandler(res, {
            data: ErrorMessage,
            exception: e
        })
    } finally {
        return next()
    }
}

export async function LoginUser(req: Request<any>, res: Response<any>, next: Function) {
    try {
        const data = res.locals.ValidatedBody

        let user = await User.findOne({
            where: {
                username: data.username
            }
        })

        if (!user) return ErrorHandler(res, {
            data: `Invalid username or password`,
            statusCode: StatusCode.unauthorised
        })

        const ValidPassword: boolean = await CheckPassword(user, data.password)
        if (!ValidPassword) return ErrorHandler(res, {
            data: `Invalid username or password`,
            statusCode: StatusCode.unauthorised
        })

        const AccountValidity: LoginEnum = CheckAccountValidity(user)
        if (!AccountValidity) return ErrorHandler(res, {
            data: `Account is not activated`,
            statusCode: StatusCode.unauhtorised
        })

        const TwoFactorAuthRequired: boolean = Check2FA(user)
        if (!TwoFactorAuthRequired) {
            const Token: string = await SignIn(user)

            res.locals.UserData = user
            res.locals.Response = {
                data: Token,
                meta: {
                    code: LoginEnum.success,
                    message: LoginEnum[LoginEnum.success]
                }
            }
        } else {
            let uuid: string = GenerateToken()
            let jwtToken = await SignJWT({
                id: user.getDataValue('id'),
                username: user.getDataValue('username'),
                recovery_token: uuid,
                recovery_method: user.getDataValue('verification_preference')
            })
    
            SetRedis(uuid, jwtToken)
            SetRedis(`${user.getDataValue('id')}_CURRENT_2FA`, user.getDataValue('verification_preference'))
    
            res.locals.UserData = user
            res.locals.Response = {
                data: jwtToken,
                meta: {
                    code: LoginEnum.twoFactorRequired,
                    message: LoginEnum[LoginEnum.twoFactorRequired]
                }
            }
        }
    } catch (e) {
        const ErrorMessage = `Unable to Insert to table Check for data ${JSON.stringify(req.body)}`
        LogService.write({
            data: ErrorMessage,
            title: ServiceTitle,
            verbosity: 'error'
        })
        return ErrorHandler(res, {
            data: ErrorMessage,
            exception: e
        })
    } finally {
        return next()
    }
}

export async function TwoFactorAuthUser(req: Request<any>, res: Response<any>, next: Function) {
    try {
        const user = res.locals.UserData
        
        const Token: string = await SignIn(user)

        res.locals.UserData = user
        res.locals.Response = {
            data: Token,
            meta: {
                code: LoginEnum.success,
                message: LoginEnum[LoginEnum.success]
            }
        }
    } catch (e) {
        const ErrorMessage = `Unable to Insert to table Check for data ${JSON.stringify(req.body)}`
        LogService.write({
            data: ErrorMessage,
            title: ServiceTitle,
            verbosity: 'error'
        })
        return ErrorHandler(res, {
            data: ErrorMessage,
            exception: e
        })
    } finally {
        return next()
    }
}

export function GetAvailable2FAs(req: Request<any>, res: Response<any>, next: Function) {
    let user = res.locals.UserData
    res.locals.Response = {
        data: Get2FAs(user)
    }
    return next()
}