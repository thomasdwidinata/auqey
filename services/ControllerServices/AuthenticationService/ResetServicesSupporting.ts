import { User } from "../../../model/User"
import Moment from 'moment'
import { GetRedis, RemoveRedis, SetRedis } from "../../RedisService"
import { ArgonHash } from "../../CryptoServices"

export async function CheckResetOTP(recovery_token: string, otp: string) {
    let Response: boolean = false
    let userId: string = await GetRedis(recovery_token)
    if (!userId) return false

    let user = await User.findByPk(userId)
    if (!user) return false

    const otpDb = user.getDataValue('otp')
    const otpExpiry = Moment(user.getDataValue('otp_expiry')).utc()
    if (otpDb === otp && otpExpiry < Moment().utc()) {
        Response = true
    }
    return Response
}

export async function ResetPassword(recovery_token: string, newPassword: string): Promise<boolean> {
    let Response: boolean = false

    let userId = await GetRedis(recovery_token)
    if (!userId) return false

    let user = await User.findByPk(userId)
    if (!user) return false

    RemoveRedis(recovery_token)

    user = await user.update({
        password: await ArgonHash(newPassword)
    })
    if (user) Response = true

    return Response
}

