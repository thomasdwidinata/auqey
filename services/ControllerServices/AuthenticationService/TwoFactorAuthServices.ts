import { Request, Response } from 'express'
import { GenerateOTP } from '../../CryptoServices'
import Moment from 'moment'
import { Global } from '../../../globals'
import { TwoFactorAuthTypes } from '../../../types/DbEnums/TwoFactorAuthTypes'
import { DispatchMail, ComposeMail } from '../../MailService'
import Config from '../../../config'
import StatusCode from '../../../globals/StatusCode'

export async function Send2FA(req: Request<any>, res: Response<any>, next: Function) {
    let user = res.locals.UserData
    const AuthType: TwoFactorAuthTypes = res.locals.TwoFactorAuthMiddleware 
    let updateOtp = await user.update({
        otp: await GenerateOTP(),
        otp_expiry: Moment().utc().add(15, 'minutes').format(Global.sqlDateTime)
    })
    if (!updateOtp) throw `Unable to update User OTP`

    switch(AuthType) {
        case 'EMAIL':
            const MailTitle: string = `${Global.app.app_name}: Security Code`
            DispatchMail({
                to: user.getDataValue('email'),
                from: Config.mail.user,
                subject: MailTitle,
                html: ComposeMail('2fa', {
                    title: MailTitle,
                    fullname: user.getDataValue('fullname'),
                    code: user.getDataValue('otp'),
                    company_name: Global.app.app_name
                })
            })
            res.locals.Response = {
                data: true
            }
            break
        case 'PHONE_NUMBER':
            res.locals.Response = {
                data: false
            }
            break
        default:
            res.locals.Response = {
                data: false,
                statusCode: StatusCode.bad_request,
                exception: `'${AuthType}' is not a valid authentication method for Two Factor Authentication`
            }
            break
    }
    return Response
}