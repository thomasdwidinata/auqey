import { Request, Response } from 'express'
import Joi from 'joi'
import { ErrorHandler } from '../../MainServices/ResponseHandler'
import { LogService } from '../../MainServices/LogServices'
import StatusCode from '../../../globals/StatusCode'
import { CheckResetOTP, ResetPassword } from './ResetServicesSupporting'
import { GenerateToken, GenerateOTP } from '../../CryptoServices'
import { SetRedis, GetRedis } from '../../RedisService'
import { Get2FAs } from './LoginServicesSupporting'
import { User } from '../../../model/User'
import GlobalStatusEnum from '../../../enums/GlobalStatusEnum'
import Config from '../../../config'
import { ComposeMail, DispatchMail } from '../../MailService'
import { Global } from '../../../globals'
import Moment from 'moment'

const ServiceTitle = 'Reset Services'

export function ValidateResetBody(req: Request<any>, res: Response<any>, next: Function) {
    try {
        const schema: Joi.ObjectSchema = Joi.object({
            username: Joi.string(),
            email: Joi.string(),
            phone_number: Joi.string(),
            otp: Joi.string(),
            new_password: Joi.string(),
            recovery_token: Joi.string()
        }).or('username', 'email', 'phone_number', 'otp', 'new_password', 'recovery_token').and('otp', 'new_password', 'recovery_token')

        const { error, value } = schema.validate(req.body)
        if (error) return ErrorHandler(res, {
            data: error,
            statusCode: StatusCode.bad_request
        })

        res.locals.ValidatedBody = value
    } catch (e) {
        const ErrorMessage = `Unable to Insert to table Check for data ${JSON.stringify(req.body)}`
        LogService.write({
            data: ErrorMessage,
            title: ServiceTitle,
            verbosity: 'error'
        })
        return ErrorHandler(res, {
            data: ErrorMessage,
            exception: e
        })
    } finally {
        return next()
    }
}

export function ValidateResetRequestBody(req: Request<any>, res: Response<any>, next: Function) {
    try {
        const schema: Joi.ObjectSchema = Joi.object({
            recovery_token: Joi.string().required(),
        })

        const { error, value } = schema.validate(req.body)
        if (error) return ErrorHandler(res, {
            data: error,
            statusCode: StatusCode.bad_request
        })

        res.locals.ValidatedBody = value
    } catch (e) {
        const ErrorMessage = `Unable to Insert to table Check for data ${JSON.stringify(req.body)}`
        LogService.write({
            data: ErrorMessage,
            title: ServiceTitle,
            verbosity: 'error'
        })
        return ErrorHandler(res, {
            data: ErrorMessage,
            exception: e
        })
    } finally {
        return next()
    }
}

export function ValidateResetRequestParams(req: Request<any>, res: Response<any>, next: Function) {
    try {
        const schema = Joi.string().valid('email', 'phone_number').insensitive()

        const { error, value } = schema.validate(req.params.type)
        if (error) return ErrorHandler(res, {
            data: error,
            statusCode: StatusCode.bad_request
        })

        res.locals.ValidatedParams = value.toUpperCase()
    } catch (e) {
        const ErrorMessage = `Unable to Insert to table Check for data ${JSON.stringify(req.body)}`
        LogService.write({
            data: ErrorMessage,
            title: ServiceTitle,
            verbosity: 'error'
        })
        return ErrorHandler(res, {
            data: ErrorMessage,
            exception: e
        })
    } finally {
        return next()
    }
}

export async function ValidateOTP(req: Request<any>, res: Response<any>, next: Function) {
    const value = res.locals.ValidatedBody
    let allowedToReset = await CheckResetOTP(value.recovery_token, value.otp)
    let resetStatus: boolean = false
    if (!allowedToReset) res.locals.Response = {
        data: `Wrong OTP code`,
        statusCode: StatusCode.unauthorised
    }
    else {
        resetStatus = await ResetPassword(value.recovery_token, value.new_password)
        if (!resetStatus) res.locals.Response = {
            data: `Unable to update password`,
            statusCode: StatusCode.unauthorised
        }
        else res.locals.Response = {}
    }
    return next()
}

export async function GetResetPassword(req: Request<any>, res: Response<any>, next: Function) {
    const data = res.locals.ValidatedBody
    let WhereClause = {}

    if (data.username) WhereClause = {
        username: data.username
    }
    if (data.email) WhereClause = {
        email: data.email
    }
    if (data.phone_number) WhereClause = {
        phone_number: data.phone_number
    }

    let potentialUser = await User.findOne({
        where: WhereClause
    })

    if (!potentialUser) {
        res.locals.GetResetPassword = potentialUser
        res.locals.Response = {
            data: potentialUser,
            statusCode: StatusCode.no_content
        }
    }
    else {
        let uuid = GenerateToken()
        SetRedis(uuid, `${potentialUser.getDataValue('id')}`)
        const AvailableTwoFactorAuth: any = Get2FAs(potentialUser)
        res.locals.UserData = potentialUser
        res.locals.Response = {
            data: AvailableTwoFactorAuth
        }
        res.locals.Response.data.recovery_token = uuid
    }
    return next()
}

export async function InitiatePasswordReset(req: Request<any>, res: Response<any>, next: Function) {
    let recovery_token = res.locals.ValidatedBody.recovery_token
    let type = res.locals.ValidatedParams
    try {
        let userId = await GetRedis(recovery_token)
        let user = await User.findByPk(userId)
        if (!user) {
            res.locals.Response = {
                data: `Recovery token has expired`,
                statusCode: StatusCode.unauthorised
            }
            return next()
        }
        let updateOtp = await user.update({
            otp: await GenerateOTP(),
            otp_expiry: Moment().utc().add(15, 'minutes').format(Global.sqlDateTime)
        })
        if (!updateOtp) throw `Unable to update User OTP`
    
        switch(type) {
            case 'EMAIL':
                const MailTitle = `${Global.app.app_name}: Reset Password`
                DispatchMail({
                    to: user.getDataValue('email'),
                    from: Config.mail.user,
                    subject: MailTitle,
                    html: ComposeMail('reset-password', {
                        title: MailTitle,
                        fullname: user.getDataValue('fullname'),
                        code: user.getDataValue('otp'),
                        company_name: Global.app.app_name,
                        url: `${Config.app_url}/api/v1/authentication/reset?recovery_token=${recovery_token}&otp=${user.getDataValue('otp')}` // Should be okay, since it has expiry
                    })
                })
                res.locals.Response = {
                    data: true
                }
            case 'PHONE_NUMBER':
                res.locals.Response = {
                    data: false
                }
                break
            default:
                res.locals.Response = {
                    data: false,
                    statusCode: StatusCode.bad_request,
                    exception: `'${type}' is not a valid authentication method for Two Factor Authentication`
                }
                break
        }
    } catch (e) {
        LogService.write({
            title: ServiceTitle,
            verbosity: 'error',
            message: 'An error occured while authenticating',
            data: e
        })
        res.locals.Response = {
            exception: e,
            meta: {
                status: GlobalStatusEnum.failed,
                message: GlobalStatusEnum[GlobalStatusEnum.failed]
            }
        }
    } finally {
        return next()
    }
}