import { readFileSync } from 'fs'
import Config from '../../config'
import JWT, { Algorithm, SignOptions } from 'jsonwebtoken'
import { LogService } from '../MainServices/LogServices'
import { Oauth_App } from '../../model/Oauth_App'
import JWTServiceOptionsInterface from '../../interfaces/JWTServiceOptionsInterface'

// Generating JWT RS256 Key: https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9

const DefaultPrivateKey: string = readFileSync(Config.jwt.key_path, 'utf8')
const DefaultPublicKey: string = readFileSync(`${Config.jwt.key_path}.pub`, 'utf8')
const DefaultAlgorithm: Algorithm = Config.jwt.algorithm as Algorithm || 'RS256'
const DefaultExpiry: string = Config.jwt.expiry // In Milisecond

const DefaultSignOptions: SignOptions = {
    algorithm: DefaultAlgorithm,
    expiresIn: DefaultExpiry
}

interface JWTSecretKeyInterface {
    identifier?: string,
    private: string,
    public: string,
    algorithm: Algorithm
}

export function GetDefaultJWTKey(): string { // Private Key
    if (DefaultPrivateKey === null) LogService.write({
        verbosity: 'error',
        message: `Unable to read JWT Private Key. Make sure you generate the key with algorithm '${Config.jwt.algorithm}'`
    })
    return DefaultPrivateKey
}

async function GetSecretConfig(identifier: string): Promise<JWTSecretKeyInterface> {
    let key: JWTSecretKeyInterface = {
        private: DefaultPrivateKey,
        public: DefaultPublicKey,
        algorithm: DefaultAlgorithm
    }
    
    if (identifier) {
        let dbKey = await Oauth_App.findOne({
            where: {
                identifier,
                status: 'VALID'
            }
        })
        if (!dbKey) throw `JWT_Key with given ID is not available on server`
        else key = {
            identifier,
            private: dbKey.getDataValue('our_private'),
            public: dbKey.getDataValue('their_public'),
            algorithm: dbKey.getDataValue('algorithm')
        }
    }
    return key
}

export async function SignJWT(payload: any, options: JWTServiceOptionsInterface = {signOptions: DefaultSignOptions}): Promise<string> { // Sign an information from this server
    let SecretConfig: JWTSecretKeyInterface = await GetSecretConfig(options.identifier ? options.identifier : '')

    if (!options.signOptions!.algorithm) options!.signOptions!.algorithm = DefaultAlgorithm
    if (!options.signOptions!.expiresIn) options!.signOptions!.expiresIn = DefaultExpiry

    return JWT.sign(payload, SecretConfig.private, options.signOptions)
}

export async function VerifyJWT(payload: any, identifier: string = ''): Promise<any> { // Used to verify a response another server
    let SecretConfig: JWTSecretKeyInterface = await GetSecretConfig(identifier)

    return JWT.verify(payload, SecretConfig.public, {
        algorithms: [ SecretConfig.algorithm ]
    })
}