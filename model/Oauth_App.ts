import { Sequelize, DataTypes, Model } from 'sequelize'
import Moment from 'moment'
export class Oauth_App extends Model {}

export function LoadModel(sequelize: Sequelize) {
	Oauth_App.init({
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		identifier: {
			allowNull: false,
			type: DataTypes.STRING(32),
			unique: true,
			validate: {
				is: /^\w{4,}$/ // Has 4 chars or more
			}
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING(50)
		},
		comment: {
			allowNull: true,
			type: DataTypes.TEXT,
		},
		our_private: {
			allowNull: false,
			type: DataTypes.STRING(255),
			comment: 'Our private key path. Other servers should have our public key and verify our token using the public key. Never store private keys on Database to prevent DB breach.'
		},
        their_public: {
            allowNull: true,
			type: DataTypes.TEXT,
			comment: 'Their public key. Other servers will sign their response using their private key and we verify it using public key. You can simply save the public key in DB as it is public.'
		},
		algorithm: {
			allowNull: false,
			type: DataTypes.STRING(64)
		},
		status: {
			allowNull: false,
			type: DataTypes.ENUM('VALID', 'REVOKED')
        },
		createdAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('createdAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		},
		updatedAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('updatedAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		}
	}, {
		tableName: 'Oauth_App',
		paranoid: true,
		sequelize
	})
}