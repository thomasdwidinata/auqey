import { Sequelize, DataTypes, Model } from 'sequelize'
import Moment from 'moment'
export class User extends Model {}

export function LoadModel(sequelize: Sequelize) {
	User.init({
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		username: {
			allowNull: false,
			type: DataTypes.STRING(50),
			unique: true,
			validate: {
				is: /^\w{4,}$/ // Has 4 chars or more
			}
		},
		fullname: {
			allowNull: false,
			type: DataTypes.STRING(128)
		},
		email: {
			allowNull: true,
			type: DataTypes.STRING(255),
			validate: {
				is: /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/g // Is a valid email address
			},
			comment: 'This field should be available on User_Informations, this field is intendedo for storing primary information'
		},
		email_verified: {
			allowNull: false,
			type: DataTypes.ENUM('FALSE', 'TRUE', '2FA'),
			defaultValue: 'FALSE'
		},
		phone_number: {
			allowNull: true,
			type: DataTypes.STRING(50),
			validate: {
				is: /^[\+]{1}[0-9]*[-]{1}[0-9]+/g // Is a valid phone number
			},
			comment: 'This field should be available on User_Informations, this field is intendedo for storing primary information'
		},
		phone_number_verified: {
			allowNull: false,
			type: DataTypes.ENUM('FALSE', 'TRUE', '2FA'),
			defaultValue: 'FALSE'
		},
		password: {
			allowNull: false,
			type: DataTypes.STRING(255),
		},
		password_expiry: {
			type: DataTypes.DATE
		},
		google_auth: {
			allowNull: true,
			type: DataTypes.STRING(255)
		},
		verification_preference: {
			allowNull: true,
			type: DataTypes.ENUM('EMAIL', 'GOOGLE_AUTH', 'PHONE_NUMBER')	
		},
		otp: {
			type: DataTypes.STRING(9),
			allowNull: true,
			comment: 'Last OTP sent by our server to email or phone_number. google_auth does not use this.'
		},
		otp_expiry: {
			type: DataTypes.DATE,
			allowNull: true
		},
		user_status: {
			allowNull: false,
			type: DataTypes.ENUM('ACTIVE', 'FROZEN', 'TEMPORARY'),
			comment: 'Used to distinguish currently active, frozen, and temporary accounts.'
		},
		createdAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('createdAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		},
		updatedAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('updatedAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		}
	}, {
		tableName: 'User',
		paranoid: true,
		sequelize
	})
}