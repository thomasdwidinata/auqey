import { Sequelize, DataTypes, Model } from 'sequelize'
import Moment from 'moment'
export class User_Information extends Model {}

export function LoadModel(sequelize: Sequelize) {
	User_Information.init({
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		value: {
			allowNull: false,
			type: DataTypes.TEXT,
        },
		createdAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('createdAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		},
		updatedAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('updatedAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		}
	}, {
		tableName: 'User_Information',
		paranoid: true,
		sequelize
	})
}