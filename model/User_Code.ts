import { Sequelize, DataTypes, Model } from 'sequelize'
import Moment from 'moment'

export class User_Code extends Model {}

export function LoadModel(sequelize: Sequelize) {
	User_Code.init({
		token: {
			allowNull: false,
			type: DataTypes.STRING(12),
			unique: 'user_token'
		},
		used: {
			allowNull: false,
			type: DataTypes.BOOLEAN,
			defaultValue: false
		},
		createdAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('createdAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		},
		updatedAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('updatedAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		}
	}, {
		sequelize,
		tableName: 'User_Code'
	})
}