import { Sequelize, DataTypes, Model } from 'sequelize'
import Moment from 'moment'
export class User_Information_Type extends Model {}

export function LoadModel(sequelize: Sequelize) {
	User_Information_Type.init({
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING(50),
			unique: true,
			validate: {
				is: /^\w{4,}$/ // Has 4 chars or more
			},
			comment: 'Human readable Data Types'
		},
		internal_id: {
			allowNull: false,
			type: DataTypes.STRING(50),
			unique: true,
			validate: {
				is: /^\w{4,}$/ // Has 4 chars or more
			},
			comment: 'Used on the programming section'
		},
        comment: {
            allowNull: true,
            type: DataTypes.TEXT
        },
		createdAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('createdAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		},
		updatedAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('updatedAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		}
	}, {
		tableName: 'User_Information_Type',
		paranoid: true,
		sequelize
	})
}