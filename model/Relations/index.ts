/**
 * Define your Relations here instead on each of the Models to prevent conflict
 */

import { User } from '../User'
import { User_Code } from '../User_Code'
import { User_Token } from '../User_Token'
import { Oauth_App } from '../Oauth_App'
import { User_Information } from '../User_Information'
import { User_Information_Type } from '../User_Information_Types'

export = () => {
    User.hasMany(User_Code)
    User_Code.belongsTo(User)
    
    User.hasMany(User_Token, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })
    User_Token.belongsTo(User, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })

    Oauth_App.hasMany(User_Token, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })
    User_Token.belongsTo(Oauth_App, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' })

    User_Information_Type.hasMany(User_Information)
    User_Information.belongsTo(User_Information_Type)

    User.hasMany(User_Information)
    User_Information.belongsTo(User)
}
