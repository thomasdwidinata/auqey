import { Sequelize, DataTypes, Model } from 'sequelize'
import Moment from 'moment'

export class User_Token extends Model {}

export function LoadModel(sequelize: Sequelize) {
	User_Token.init({
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		token: {
			allowNull: false,
			type: DataTypes.TEXT,
		},
		token_expiry: {
			allowNull: false,
			type: DataTypes.DATE
		},
		refresh_token: {
			allowNull: false,
			type: DataTypes.TEXT,
		},
		refresh_token_expiry: {
			allowNull: false,
			type: DataTypes.DATE	
		},
		createdAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('createdAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		},
		updatedAt: {
			type: DataTypes.DATE,
			defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
			get() {
				return Moment(this.getDataValue('updatedAt')).format('YYYY-MM-SS hh:mm:ss')
			}
		}
	}, {
		sequelize,
		tableName: 'User_Token'
	})
}