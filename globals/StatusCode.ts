'use strict'

import StatusCodeInterface from '../interfaces/StatusCodeInterface'

interface SCI {
    [key: string]: StatusCodeInterface
}

let StatusCode: SCI = {
    'success': {
        http_code: 200,
        status_code: 200,
        message: 'Success'
    },
    'created': {
        http_code: 201,
        status_code: 201,
        message: 'Created'
    },
    'accepted': {
        http_code: 202,
        status_code: 202,
        message: 'Accepted'
    },
    'no_content': {
        http_code: 200, // Don't change the Code,! Due to Express will literally ignore every response you give
        status_code: 204,
        message: 'Success, no content'
    },
    'reset_content': {
        http_code: 205,
        status_code: 205,
        message: 'Success, reset content'
    },
    'multi_status': {
        http_code: 207,
        status_code: 207,
        message: 'Multi-Status'
    },
    'see_other': {
        http_code: 303,
        status_code: 303,
        message: 'See Other'
    },
    'bad_request': {
        http_code: 400,
        status_code: 400,
        message: 'Bad Request'
    },
    'unauthorised': {
        http_code: 401,
        status_code: 401,
        message: 'Unauthorised'
    },
    'forbidden': {
        http_code: 403,
        status_code: 403,
        message: 'Forbidden'
    },
    'not_found': {
        http_code: 404,
        status_code: 404,
        message: 'Not Found'
    },
    'session_expired': {
        http_code: 440,
        status_code: 440,
        message: 'Session Expired'
    },
    'internal_server_error': {
        http_code: 500,
        status_code: 500,
        message: 'Internal Server Error'
    }
}

export = StatusCode