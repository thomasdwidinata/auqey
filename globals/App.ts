'use strict'

import gitRepoInfo from 'git-repo-info'

const hash = gitRepoInfo().sha.substring(0, 10) || null

export let App = {
    app_id: 'auqey',
    app_name: 'AuQey',
    version: '0.0.1 Alpha',
    git_hash: hash
}