'use strict'

import { App } from './App'
import StatusCode from './StatusCode'

export let Global = {
    app: App,
    status_code: StatusCode,
    /** Add your custom Global Variables below and import this file using `import '@global'` */
    sqlDateTime: 'YYYY-MM-DD HH:mm:ss',
    sqlDate: 'YYYY-MM-DD'
}