import chai from 'chai'
import querystring from 'querystring'
import server from '../'
import jwt from 'jsonwebtoken'
const chaihttp = require('chai-http')
const assert = chai.assert
const expect = chai.expect
const should = chai.should
const CheckBody = require('./checkBody')
const Db = require('../services/db');
/*
Db.connect();

chai.use(chaihttp)
chai.use(require('chai-url'))

const fs = require('fs')
const path = __dirname + "/upload.png"
if (!fs.existsSync(path)) throw "upload.png must exists on current folder."

// SET YOUR ENV HERE
const credentials = {
  username: 'omoshiroi',
  password: '1q2w3e'
}
const jwt_encryption = "JWTFAILSAFE678ad678y7asIAiuA:7j2031840ks}"

// Global variables, this thing declared here just for standarisation
let url = ``;
let obj = {};
let query = {};
let dbResponse = {};

let token = ``;
const deviceOSes = ["iOS", 'Android'];
const deviceTypes = {
  iOS: ['iPhone7,1', 'iPhone12,1', 'iPhone8,2', 'iPad7,2'],
  Android: ['Pixel 4A', 'Xperia Z3+', "Galaxy S20", "RedMi 4A", 'Huawei P30 Pro']
}
const deviceVersions = {
  iOS: ["iOS 12.2.4", "iOS 13.4", "iOS 10.3.3"],
  Android: ['Android 7.1.1', 'Android 8.1', 'Android 10.1', "Android 9.3"]
}
let deviceOS = deviceOSes[Math.floor(Math.random() * deviceOSes.length)];
let deviceVersion = deviceVersions[deviceOS][Math.floor(Math.random() * deviceVersions[deviceOS].length)]
let deviceType = deviceTypes[deviceOS][Math.floor(Math.random() * deviceTypes[deviceOS].length)]
let userData = null;

describe('Live Test', () => {
  it('Test Server', (done) => {
    url = `/api/test`;
    chai.request(server)
      .get(url)
      .end((err, res) => {
        expect(res).to.have.status(200) // HTTP Status
        expect(res.body.status).to.be.a('string')
        expect(res.body.status).to.equal('success')
        done()
      })
  })
})

describe('Authentication', () => {
  it('Login (POST: /api/auth/login)', async () => {
    url = '/api/auth/login'
    obj = {
      username: credentials.username,
      password: credentials.password,
      device_os: deviceOS,
      device_type: deviceType,
      device_version: deviceVersion,
      device_uuid: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    }
    let res = await chai.request(server)
      .post(url)
      .send(obj)
    CheckBody(res)
    expect(res.body.data).to.have.keys('user_id', 'username', 'fullname', 'pic', 'region_code', 'reference_code', 'type', 'last_login', 'permission', 'token', 'token_expired', 'expires_in', 'status')
    expect(res.body.data.user_id).to.be.a('number')
    expect(res.body.data.pic).to.have.protocol('https')
    expect(res.body.data.pic).to.have.string('ap-southeast-1.amazonaws.com')
    expect(res.body.data.token).to.be.a('string')
    userData = res.body.data
    token = `Bearer ${res.body.data.token}`
  }).timeout(5000)

  it('Test Server Version', (done) => {
    url = `/api/test/version`;
    chai.request(server)
      .get(url)
      .set('Authorization', token)
      .end((err, res) => {
        expect(res).to.have.status(200) // HTTP Status
        expect(res.body.status).to.be.a('string')
        expect(res.body.status).to.equal('success')
        done()
      })
  })

  it('Middleware check', async () => {
    let data = await Db.query('SELECT mt.user_id, um.username, um.email, um.password, um.location_id, l.code as location_code, l.region_code, l.label as location_label, l.fullname as location_fullname, mt.token, mt.token_expired, um.type, um.photo_url, um.status FROM mobile_token mt JOIN user_mobile um ON mt.user_id = um.user_id JOIN location l ON um.location_id = l.id WHERE mt.token = ? AND um.deleted_at IS NULL', token.split(' ')[1])
    if(data.length < 1) throw "The middleware might have problems. Unable to get response from DB using current token."
    else {
      userData.location_id = data[0].location_id
      userData.location_fullname = data[0].location_fullname
      userData.location_label = data[0].location_label
    }
  })

  it('Use Refresh Token (POST: /api/auth/exchange)', async () => {
    url = '/api/auth/exchange'
    let refresh_token = jwt.verify(userData.token, jwt_encryption)
    if(!refresh_token) throw "JWT Key is invalid. Cannot decrypt Token."
    refresh_token = refresh_token.refresh_token
    obj = {
      refresh_token: refresh_token,
      user_id: userData.user_id,
      device_os: deviceOS,
      device_type: deviceType,
      device_version: deviceVersion,
      device_uuid: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
    }
    let res = await chai.request(server)
      .post(url)
      .send(obj)
    CheckBody(res)
    expect(res.body.data).to.have.keys('user_id', 'last_login', 'token', 'token_expired', 'expires_in', 'status')
    expect(res.body.data.user_id).to.be.a('number')
    expect(res.body.data.token).to.be.a('string')
    userData.token = res.body.data.token
    userData.token_expired = res.body.data.token_expired
    userData.lastLogin = res.body.data.last_login
    token = `Bearer ${res.body.data.token}`
  })
})

describe('Profile Management', () => {
  it('Get Profile (GET: /api/profile)', (done) => {
    url = '/api/profile'
    chai.request(server)
      .get(url)
      .set('Authorization', token)
      .send(obj)
      .end((err, res) => {
        CheckBody(res)
        expect(res.body.data).to.have.keys('username', 'fullname', 'email', 'name', 'photo_url', 'region_code', 'partshop_code', 'type', 'permission', 'reference_code', 'lat', 'lng')
        expect(res.body.data.photo_url).to.have.protocol('https')
        expect(res.body.data.photo_url).to.have.string('ap-southeast-1.amazonaws.com')
        userData.pic = res.body.data.photo_url
        done()
      })
  })

  it(`Get Profile Picture (GET)`, (done) => {
    chai.request(userData.pic)
      .get('')
      .end((err, res) => {
        expect(res).to.have.status(200)
        done()
      })
  })

  it('Update Profile (POST: /api/profile)', (done) => {
    url = '/api/profile'
    chai.request(server)
      .post(url)
      .set('Authorization', token)
      .field('name', 'Dr. Robotnik')
      .field('password', '1q2w3e')
      .attach('photo', path, 'test.png')
      .end((err, res) => {
        CheckBody(res)
        done()
      })
  })

  it('Get Merchants (GET: /api/profile/merchant)', (done) => {
    url = '/api/profile/merchant'
    query = {
      page: 1,
      search: "abc"
    }
    chai.request(server)
      .get(`${url}?${querystring.encode(query)}`)
      .set('Authorization', token)
      .end((err, res) => {
        CheckBody(res)
        done()
      })
  })
})

describe('Dashboard or Home', () => {
  it('Get Home Details (GET: /api/home)', (done) => {
    url = '/api/home'
    chai.request(server)
      .get(url)
      .set('Authorization', token)
      .send(obj)
      .end((err, res) => {
        CheckBody(res)
        expect(res.body.data).to.have.keys('news', 'item', 'incentive')

        expect(res.body.data.news).to.be.an('array')
        for (let news of res.body.data.news) expect(news).to.have.keys('id', 'title', 'news', 'categories', 'status', 'access', 'pic', 'region', 'created_at')

        expect(res.body.data.item).to.be.an('array')
        for (let item of res.body.data.item) expect(item).to.have.keys('id', 'title', 'news', 'categories', 'status', 'access', 'pic', 'region', 'created_at')

        expect(res.body.data.incentive).to.be.a('number')
        done()
      })
  })
})

describe('Reseller Management', () => {
  it('Add new Reseller (POST: /api/management/merchant)', async () => {
    url = '/api/management/merchant';
    obj = {
      username: Math.random().toString(20).replace(/[^a-z]+/g, '').substr(0, 100),
      fullname: Math.random().toString(20).replace(/[^a-z]+/g, '').substr(0, 100),
      address: "Jalan Gaya Motor 2 No. 45, Jakarta",
      password: "1q2w3e-",
      phone_number: "081111111",
      email: "aaa@aaa.com"
    }
    let res = await chai.request(server)
      .post(url)
      .set('Authorization', token)
      .send(obj)
    CheckBody(res)
    dbResponse = await Db.query(`SELECT um.user_id, um.username, um.email, um.phone_number, um.type, l.code, l.alias_code, l.region_code, l.fullname, l.address, l.lat, l.lng, um.reference_code FROM location l RIGHT JOIN user_mobile um ON l.id = um.location_id WHERE (l.id = ? OR l.parent_id = ?) AND um.type = "RESELLER" AND um.username = ?`, [userData.location_id, userData.location_id, obj.username])
    if(dbResponse.length < 1) throw "The API responded 200 but not available on the database."
    else {
      obj = dbResponse[0]
    }
  })

  it('Edit Reseller (PUT: /api/management/merchant)', async () => {
    url = '/api/management/merchant';
    obj = {
      username: obj.username,
      user_id: obj.user_id,
      fullname: "New Merchant",
      address: "Jalan ABCDEF",
      phone_number: "082222222",
      email: "foobar@google.com"
    }
    let res = await chai.request(server)
      .put(url)
      .set('Authorization', token)
      .send(obj)
    CheckBody(res)
    dbResponse = await Db.query(`SELECT um.user_id, um.username, um.email, um.phone_number, um.type, l.code, l.alias_code, l.region_code, l.fullname, l.address, l.lat, l.lng, um.reference_code FROM location l RIGHT JOIN user_mobile um ON l.id = um.location_id WHERE (l.id = ? OR l.parent_id = ?) AND um.type = "RESELLER" AND um.username = ?`, [userData.location_id, userData.location_id, obj.username])
    if(dbResponse.length < 1) throw "The API responded 200 but not available on the database."
    else if(dbResponse[0].fullname !== obj.fullname) throw "The API responded 200 but the database was not modified."
  })

  it('Delete Reseller (DELETE: /api/management/merchant)', async () => {
    url = `/api/management/merchant/${obj.user_id}`;
    let res = await chai.request(server)
      .delete(url)
      .set('Authorization', token)
      .send(obj)
    CheckBody(res)
    dbResponse = await Db.query(`SELECT * FROM location l RIGHT JOIN user_mobile um ON l.id = um.location_id WHERE l.id = ? AND um.type = "RESELLER" AND um.username = ?`, [userData.user_id, obj.username])
    if(dbResponse.length > 0) throw "The API responded 200 but was not deleted from the database."
  })
})

describe('Stock', () => {
  it('Get Stock Count (GET: /api/stock/count)', (done) => {
    url = '/api/stock/count'
    chai.request(server)
      .get(url)
      .set('Authorization', token)
      .send(obj)
      .end((err, res) => {
        CheckBody(res)
        expect(res.body.data).to.have.keys('box', 'product')

        expect(res.body.data.box).to.be.a('number')
        expect(res.body.data.product).to.be.a('number')
        done()
      })
  })

  let lastPage = 0;
  let temp = {};

  it('Get Stock List (GET: /api/stock) Page 1', (done) => {
    url = '/api/stock'
    chai.request(server)
      .get(url)
      .set('Authorization', token)
      .send(obj)
      .end((err, res) => {
        CheckBody(res, true)

        expect(res.body.data).to.be.an('array')

        for (let stock of res.body.data) {
          expect(stock).to.have.keys('qrcode', 'qrcode_type', 'sku', 'product_type_name', 'product_variant', 'scan_time', 'count')
          expect(stock.qrcode).to.be.a('string')
          expect(stock.qrcode_type).to.be.a('string')
          expect(stock.scan_time).to.be.a('string')
          expect(stock.count).to.be.a('number')
        }

        temp = res.body.data
        lastPage = res.body.meta.lastPage

        if (lastPage > 1)
          describe(`Get Stock List Pagination (GET: /api/stock)`, () => {
            for (let i = 2; i <= lastPage; ++i) {
              it(`Get Stock List (GET: /api/stock) Page ${i}`, async () => {
                url = '/api/stock'
                let res = await chai.request(server)
                  .get(`${url}?${querystring.encode({page: i})}`)
                  .set('Authorization', token)
                  .send(obj)
                CheckBody(res, true)

                expect(res.body.data).to.be.an('array')

                for (let stock of res.body.data) {
                  expect(stock).to.have.keys('qrcode', 'qrcode_type', 'sku', 'product_type_name', 'product_variant', 'scan_time', 'count')
                  expect(stock.qrcode).to.be.a('string')
                  expect(stock.qrcode_type).to.be.a('string')
                  expect(stock.scan_time).to.be.a('string')
                  expect(stock.count).to.be.a('number')
                }

                expect(res.body.data).to.not.deep.equal(temp)

                temp = res.body.data
              })
              if (i === 5) i = lastPage - 1 // Maximum 5 loops
            }
          })
        done()
      })
  })
})

*/