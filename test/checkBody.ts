'use strict';

import chai from 'chai'
const expect        = chai.expect

import { Request } from 'express'

module.exports = (res: Request<any>, withMeta = false) => {
    // expect(res).to.have.status(200)
    expect(res.body.code).to.be.a('number')
    expect(res.body.message).to.equal('Success')
    if(withMeta) {
        expect(res.body.meta).to.be.a('object')
        expect(res.body.meta.currentPage).to.be.a('number')
        expect(res.body.meta.total).to.be.a('number')
        expect(res.body.meta.lastPage).to.be.a('number')
    }
    // expect(res.body.data).to.be.a('object') || expect(res.body.data).to.be.an('array')
}