'use strict'

/*!
 * AuQey
 * An SSO Middleware used to be a microservice
 * Copyright © Thomas Dwi Dinata
 */

/** Initialisation User Feedback */

/** Import important dependencies such as Web Framework and several native Node.js Libraries */
import express from 'express'
import http from 'http'
import bodyparser from 'body-parser'

/** Import Dependencies that uses Route Injection */
import { ErrorHandler } from './services/MainServices/ErrorHandler'
import { RouteScan } from './services/MainServices/RouterInit'

/** Import Logging, Database, and Redis Services */
import { InitLogService, LogService } from './services/MainServices/LogServices'
import { InitDatabase } from './services/DatabaseServices'
import { VerifyRedis } from './services/RedisService'

/** Import non-Typescript modules */
import cors from 'cors'
import morgan from 'morgan'

/** Import Global Variables and Configurations got from .env file */
import { Global } from './globals'
import Config from './config'

/** Initialise Logging */
InitLogService(Config.log_type)

/** Give user feedback that is currently loading */
LogService.log(`${Global.app.app_name}\nId: ${Global.app.app_id}\nVersion: ${Global.app.version}\nGit Hash: ${Global.app.git_hash}\n`)
LogService.info(`Initialising App...`)

/** Initialise Server with Extensions and tweak the configuration */
LogService.info(`Initialising Express Framework...`)
const app       = express()
const server    = http.createServer(app) 
app.set('trust proxy', true)
app.use(cors())
app.use(morgan('combined', {
    skip: function (_req: any, res: { statusCode: number }) {
        return res.statusCode < 400 || res.statusCode === 206;
    }
}))
app.use(bodyparser.json({
    limit: `${Config.max_request_size}mb`
}))
app.use(bodyparser.urlencoded({
    limit: `${Config.max_request_size}mb`,
    extended: true
}));

/** Traverse the routes folder */
LogService.info(`Scanning Routes...`)
const routes = RouteScan(app)
LogService.info(`Found routes:`)
for (let route of routes)
    LogService.info(`\t> ${route.url}`)

/** Embedd 404 and Error 500 */
ErrorHandler(app)

/** Connect to Database */
LogService.write({
    icon: '⏳',
    verbosity: 'info',
    message: `Connecting to Database...`
})
InitDatabase()

/** Connect to Redis */
LogService.write({
    icon: '⏳',
    verbosity: 'info',
    message: `Connecting to Redis...`
})
VerifyRedis()

/** Start to Serve */
server.listen(Config.http_port, () => LogService.write({
    icon: '✅',
    message: `Server is listening on port ${Config.http_port}...`,
    verbosity: 'info'
}));

// --
export default app