import { Request, Response } from 'express'
import { SuccessHandler, ErrorHandler } from '../services/MainServices/ResponseHandler'
import { LogService } from '../services/MainServices/LogServices'
import Joi from 'joi'
import StatusCode from '../globals/StatusCode'
import { User } from '../model/User'
import { GetRedis } from '../services/RedisService'
import { VerifyJWT } from '../services/JWTService'

const MiddlewareTitle: string = "AuthenticationMiddleware"

export function AuthenticationMiddleware (_req: Request<any>, res: Response<any>, next: any) {
    try {
        // Implement your Authentication Middleware here
        let coming_soon_message = 'WIP: Not yet implemented. Come back later!'
        console.info(coming_soon_message)
        return SuccessHandler(res, {
            data: coming_soon_message
        })
    } catch (e) {
        LogService.write({
            verbosity: 'error',
            title: MiddlewareTitle,
            message: `An error occured while guarding APIs`,
            data: e
        })
        return ErrorHandler(res, {
            exception: e
        })
    }
}

export async function TwoFactorAuthMiddleware(req: Request<any>, res: Response<any>, next: any, required: boolean) {
    try {
        const schema = Joi.object({
            recovery_token: Joi.string().required()
        }).unknown()
        const { error, value } = schema.validate(req.body)
        if (error) {
            if (required) return ErrorHandler(res, {
                data: error,
                statusCode: StatusCode.bad_request
            })
            else return next()
        }

        let user_token: string = await GetRedis(value.recovery_token)
        if (!user_token) return ErrorHandler(res, {
            data: 'Recovery token is invalid or has expired',
            statusCode: StatusCode.unauthorised
        })

        let user_info = await VerifyJWT(user_token)
        if (!user_info)
            return ErrorHandler(res, {
                data: `Recovery token has expired`,
                statusCode: StatusCode.unauthorised
            })
        let current_user_2fa = await GetRedis(`${user_info.id}_CURRENT_2FA`)
        if (user_info.recovery_method !== current_user_2fa) return ErrorHandler(res, {
            data: `Invalid 2FA Method`,
            statusCode: StatusCode.unauthorised
        })

        let user = await User.findOne({
            where: {
                id: user_info.id
            }
        })
        if (!user) {
            return ErrorHandler(res, {
                data: `Recovery token has expired`,
                statusCode: StatusCode.unauthorised
            })
        } else {
            res.locals.UserData = user
            res.locals.TwoFactorAuthMiddleware = current_user_2fa
            return next()
        }
    } catch (e) {
        LogService.write({
            verbosity: 'error',
            title: MiddlewareTitle,
            message: `An error occured while guarding the 2FA APIs`,
            data: e
        })
        return ErrorHandler(res, {
            exception: e
        })
    }
}

export async function TwoFactorAuthMandatoryMiddleware(req: Request<any>, res: Response<any>, next: any) {
    try {
        return TwoFactorAuthMiddleware(req, res, next, true)
    } catch (e) {
        LogService.write({
            verbosity: 'error',
            title: MiddlewareTitle,
            message: `An error occured while guarding the 2FA APIs`,
            data: e
        })
        return ErrorHandler(res, {
            exception: e
        })
    }
}

export async function TwoFactorAuthOptionalMiddleware(req: Request<any>, res: Response<any>, next: any) {
    try {
        return TwoFactorAuthMiddleware(req, res, next, false)
    } catch (e) {
        LogService.write({
            verbosity: 'error',
            title: MiddlewareTitle,
            message: `An error occured while guarding the 2FA APIs`,
            data: e
        })
        return ErrorHandler(res, {
            exception: e
        })
    }
}