'use strict'

interface ResponseInterface {
    code: number,
    message: string,
    data?: any,
    meta?: any
}

export = ResponseInterface