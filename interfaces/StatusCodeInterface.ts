'use strict'

interface StatusCodeInterface {
    http_code: number,
    status_code: number,
    message: string
}

export = StatusCodeInterface