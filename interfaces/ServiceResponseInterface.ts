import StatusCodeInterface from './StatusCodeInterface'

interface ResponseMetaInterface {
    status: string | number,
    message: string,
    [key: string]: any // Allow other objects
}

// This is intended to be used on the last call of a services on Controller to make sure Controller is not bloated
interface ServiceRepsonseInterface {
    data?: any,
    meta?: ResponseMetaInterface,
    exception?: any,
    statusCode?: StatusCodeInterface
}

export = ServiceRepsonseInterface