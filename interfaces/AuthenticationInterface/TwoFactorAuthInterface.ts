export interface TwoFactorAuthInterface {
    otp: string,
    type: string,
    recovery_token: string
}