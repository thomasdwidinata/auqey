export interface ResetPasswordDataInterface {
    email?: string,
    username?: string,
    phone_number?: string
}