import { Request } from 'express'
import { ConsoleTypes } from '../types/ConsoleTypes'

interface LogInterface {
    request?: Request<any>,
    id?: string,
    icon?: string,
    verbosity?: ConsoleTypes,
    title?: string,
    message?: string,
    data?: any,
    ticket?: string
}

export = LogInterface