import { SignOptions } from 'jsonwebtoken'

interface JWTServiceOptionsInterface {
    identifier?: string,
    signOptions?: SignOptions
}

export = JWTServiceOptionsInterface