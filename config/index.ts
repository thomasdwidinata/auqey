require('dotenv').config();

function GetEnvironment() {
    if (typeof(process.env.NODE_ENV) !== 'string') process.env.NODE_ENV = 'development' 
    switch(process.env.NODE_ENV) {
        case 'development':
        case 'production':
            return process.env.NODE_ENV
        default:
            return 'development' // Any Invalid settings will be considered as development
    }
}

function GetLoggingType() {
    if (typeof(process.env.LOG_TYPE) !== 'string') process.env.LOG_TYPE = 'sqlite' 
    switch(process.env.LOG_TYPE) {
        case 'file':
        case 'sqlite':
        case 'sequelize':
        case 'mysql':
            return process.env.LOG_TYPE
        default:
            return 'sqlite' // Any Invalid settings will be considered as development
    }
}

const Config = {
    http_port: process.env.HTTP_PORT || 5000,
    max_request_size: process.env.MAX_REQUEST_SIZE ? parseInt(process.env.MAX_REQUEST_SIZE) : 1, // In Megabyte,
    max_log_size: process.env.MAX_LOG_SIZE ? parseInt(process.env.MAX_LOG_SIZE) : 5, // In Megabyte,
    debug: (process.env.DEBUG === 'true' ? true : false) || false,
    environment: GetEnvironment(),
    database: {
        dialect: process.env.DB_DIALECT || 'mysql',
        host: process.env.DB_HOST || '127.0.0.1',
        port: process.env.DB_PORT || 3306,
        name: process.env.DB_NAME || 'sso',
        username: process.env.DB_USERNAME || 'root',
        password: process.env.DB_PASSWORD || 'root',
    },
    redis: {
        host: process.env.REDIS_HOST || 'localhost',
        port: process.env.REDIS_PORT ? parseInt(process.env.REDIS_PORT) : 6379,
        password: process.env.REDIS_PASSWORD || '',
        ttl: process.env.REDIS_TTL ? parseInt(process.env.REDIS_TTL) : 300, // In Seconds
    },
    log_type: GetLoggingType(),
    jwt: {
        key_path: process.env.JWT_KEY_PATH ? `${__dirname}/../${process.env.JWT_KEY_PATH}` : __dirname + '/../private/jwtRS256.key',
        algorithm: process.env.JWT_ALGORITHM || 'RS256', // HS256 === HMAC SHA256, is the default for jsonwebtoken library. For this project, we use a different one
        expiry: process.env.JWT_EXPIRY || '15m' // Must comply with 'ms' package
    },
    mail: {
        host: process.env.MAIL_HOST || 'smtp-mail.outlook.com',
        port: process.env.MAIL_PORT ? parseInt(process.env.MAIL_PORT) : 587,
        secure: process.env.MAIL_SECURE === 'true' ? true : false,
        user: process.env.MAIL_USERNAME || 'sample@outlook.com',
        password: process.env.MAIL_PASSWORD || '123456', // Make sure to use only App Passwords,
        auth_type: process.env.MAIL_AUTH_TYPE || 'password',
        oauth2_key_path: process.env.MAIL_OAUTH_KEY_PATH ? `${__dirname}/../${process.env.MAIL_OAUTH_KEY_PATH}` : __dirname + '/../private/mail.json',
    },
    app_url: process.env.APP_URL || 'http://localhost'
}

export = Config