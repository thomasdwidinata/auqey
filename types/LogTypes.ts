/** Add another type if you have implement it on services/MainServices/LogServices.ts! */
type LogTypes = 'file' | 'sequelize' | 'mysql' | 'sqlite'
export = LogTypes