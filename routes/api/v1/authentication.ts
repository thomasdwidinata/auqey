import { Router } from 'express'
const router: Router = Router()

import AuthenticationController from '../../../controllers/AuthenticationController'

router.use('/', AuthenticationController)

export = router