import { Router } from 'express'
const router: Router = Router()

import TestController from '../../controllers/TestController'

router.use('/', TestController)

export = router