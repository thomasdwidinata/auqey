import { Router, Request, Response } from 'express'
const router: Router = Router()

import { SuccessHandler } from '../services/MainServices/ResponseHandler'
import { Global } from '../globals'

router.get('/', (_req: Request<any>, res: Response<any>) => {
    SuccessHandler(res, {
        data: Global.app
    })
})

export = router