import { Router, Request, Response } from 'express'
import { TwoFactorAuthMiddleware } from '../../middlewares/AuthenticationMiddleware'
import { ValidateBody, TwoFactorAuthUser, LoginUser, GetAvailable2FAs } from '../../services/ControllerServices/AuthenticationService/LoginServices'
import ValidateBodyEnum from '../../enums/AuthenticationEnums/ValidateBodyEnum'
import { EndControllerServices } from '../../services/EndControllerServices'
import { Send2FA } from '../../services/ControllerServices/AuthenticationService/TwoFactorAuthServices'
import { ValidateResetBody, ValidateResetRequestBody, ValidateOTP, ValidateResetRequestParams, GetResetPassword, InitiatePasswordReset } from '../../services/ControllerServices/AuthenticationService/ResetServices'
const router: Router = Router()

const ControllerName = 'AuthenticationController'

// WIP: Documentation Standarisation coming soon
router.post('/login', [
    (req: Request<any>, res: Response<any>, next: Function) => {
        const AuthRequired: boolean = false
        return TwoFactorAuthMiddleware(req, res, next, AuthRequired)
    },
    (req: Request<any>, res: Response<any>, next: Function) => {
        if (res.locals.TwoFactorAuthMiddleware) return ValidateBody(req, res, next, ValidateBodyEnum.TwoFactorAuth)
        else return ValidateBody(req, res, next, ValidateBodyEnum.Login)
    },
    (req: Request<any>, res: Response<any>, next: Function) => {
        if (res.locals.UserData) return TwoFactorAuthUser(req, res, next)
        else return LoginUser(req, res, next)
    },
    EndControllerServices
])

router.post('/otp', [
    (req: Request<any>, res: Response<any>, next: Function) => {
        const AuthRequired: boolean = true
        return TwoFactorAuthMiddleware(req, res, next, AuthRequired)
    },
    GetAvailable2FAs,
    EndControllerServices
])

router.post('/otp/:type', [
    (req: Request<any>, res: Response<any>, next: Function) => {
        const AuthRequired: boolean = true
        return TwoFactorAuthMiddleware(req, res, next, AuthRequired)
    },
    Send2FA
])

router.post('/reset/:type', [
    ValidateResetRequestBody,
    ValidateResetRequestParams,
    InitiatePasswordReset,
    EndControllerServices
])

router.post('/reset', [
    ValidateResetBody,
    (req: Request<any>, res: Response<any>, next: Function) => {
        const value = res.locals.ValidatedBody
        if (value.otp && value.new_password, value.recovery_token) { // If resets using API Based
            return ValidateOTP(req, res, next)      
        } else if (value.username || value.email || value.phone_number) {
            return GetResetPassword(req, res, next)
        } else return next()
    },
    EndControllerServices
])
export = router