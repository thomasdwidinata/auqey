enum LoginEnum {
    noSuchUser = 0,
    success,
    invalidPassword,
    twoFactorRequired,
    accountDisabled,
    expiredPassword
}
export = LoginEnum